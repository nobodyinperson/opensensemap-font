# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: opensensemap-font
Version: 0.0.1
Release: jolla
Summary: OpenSenseMap icon font
License: MIT
URL: https://gitlab.com/nobodyinperson/opensensemap-font
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@gmx.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: make

%description
This package provides the openSenseMap icon font for use in applications that
need to display the sensor icons.

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/fonts/*

%changelog
* Fri Sep 28 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.1-jolla
- Initial package
